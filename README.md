# tap-zengrc

`tap-zengrc` is a Singer tap for [ZenGRC](https://reciprocity.com/product/).

Built with the [Meltano Tap SDK](https://sdk.meltano.com) for Singer Taps.

## Installation

```bash
pipx install git+https://gitlab.com/gitlab-data/tap-zengrc.git
```

### Source Authentication and Authorization

Authentication works via `config.json` (added to `/.secrets` for coverage in `gitignore`)

Example
```json
{
  "base_url": "https://gitlab.api.zengrc.com/api/v2",
  "username": "<key_id>",
  "password": "<key_secret>"
}
```

## Usage

You can easily run `tap-zengrc` by itself or in a pipeline using [Meltano](https://meltano.com/).

### Executing the Tap Directly

```bash
poetry run tap-zengrc --config .secrets/config.json
```

## Developer Resources

### Initialize your Development Environment

```bash
pipx install poetry
poetry install
```

### Testing with [Meltano](https://www.meltano.com)

_**Note:** This tap will work in any Singer environment and does not require Meltano.
Examples here are for convenience and to streamline end-to-end orchestration scenarios._

Your project comes with a custom `meltano.yml` project file already created. Open the `meltano.yml` and follow any _"TODO"_ items listed in
the file.

Next, install Meltano (if you haven't already) and any needed plugins:

```bash
# Install meltano
pipx install meltano
# Initialize meltano within this directory
cd tap-zengrc
meltano install
```

Now you can test and orchestrate using Meltano:

```bash
# Test invocation:
meltano invoke tap-zengrc --version
# OR run a test `elt` pipeline:
meltano elt tap-zengrc target-jsonl
```

### SDK Dev Guide

See the [dev guide](https://sdk.meltano.com/en/latest/dev_guide.html) for more instructions on how to use the SDK to 
develop your own taps and targets.
