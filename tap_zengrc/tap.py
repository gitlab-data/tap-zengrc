"""ZenGRC tap class."""

from typing import List

from singer_sdk import Tap, Stream
from singer_sdk import typing as th  # JSON schema typing helpers

from tap_zengrc.streams import (
    ZenGRCStream,
    AuditsStream,
    AssessmentsStream,
    ControlsStream,
    IssuesStream,
    ObjectivesStream,
    RequestsStream,
    RisksStream,
    VendorsStream,
    ProgramsStream,
)

STREAM_TYPES = [
    AuditsStream,
    AssessmentsStream,
    ControlsStream,
    IssuesStream,
    ObjectivesStream,
    RequestsStream,
    RisksStream,
    VendorsStream,
    ProgramsStream,
]


class TapZenGRC(Tap):
    """ZenGRC tap class."""

    name = "tap-zengrc"

    config_jsonschema = th.PropertiesList(
        th.Property("base_url", th.StringType, default="https://api.mysample.com"),
        th.Property("username", th.StringType, required=True),
        th.Property("password", th.StringType, required=True),
    ).to_dict()

    def discover_streams(self) -> List[Stream]:
        """Return a list of discovered streams."""
        return [stream_class(tap=self) for stream_class in STREAM_TYPES]
