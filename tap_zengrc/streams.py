"""Stream type classes for tap-zengrc."""

from pathlib import Path
from typing import Any, Dict, Optional, Union, List, Iterable

from singer_sdk import typing as th  # JSON Schema typing helpers

from tap_zengrc.client import ZenGRCStream

SCHEMAS_DIR = Path(__file__).parent / Path("./schemas")


class AuditsStream(ZenGRCStream):
    """Audits Object."""

    name = "audits"
    path = "/audits"
    primary_keys = ["id"]
    schema_filepath = SCHEMAS_DIR / "audits.json"


class AssessmentsStream(ZenGRCStream):
    """Assessments Object."""

    name = "assessments"
    path = "/assessments"
    primary_keys = ["id"]
    schema_filepath = SCHEMAS_DIR / "assessments.json"


class ControlsStream(ZenGRCStream):
    """Controls Object."""

    name = "controls"
    path = "/controls"
    primary_keys = ["id"]
    schema_filepath = SCHEMAS_DIR / "controls.json"


class IssuesStream(ZenGRCStream):
    """Issues Object."""

    name = "issues"
    path = "/issues"
    primary_keys = ["id"]
    schema_filepath = SCHEMAS_DIR / "issues.json"


class ObjectivesStream(ZenGRCStream):
    """Objectives Object."""

    name = "objectives"
    path = "/objectives"
    primary_keys = ["id"]
    schema_filepath = SCHEMAS_DIR / "objectives.json"


class RisksStream(ZenGRCStream):
    """Risks Object."""

    name = "risks"
    path = "/risks"
    primary_keys = ["id"]
    schema_filepath = SCHEMAS_DIR / "risks.json"


class RequestsStream(ZenGRCStream):
    """Requests Object."""

    name = "requests"
    path = "/requests"
    primary_keys = ["id"]
    schema_filepath = SCHEMAS_DIR / "requests.json"


class VendorsStream(ZenGRCStream):
    """Vendors Object."""

    name = "vendors"
    path = "/vendors"
    primary_keys = ["id"]
    schema_filepath = SCHEMAS_DIR / "vendors.json"


class ProgramsStream(ZenGRCStream):
    """Programs Object."""

    name = "programs"
    path = "/programs"
    primary_keys = ["id"]
    schema_filepath = SCHEMAS_DIR / "programs.json"